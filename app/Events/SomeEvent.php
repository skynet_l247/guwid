<?php

namespace GuWID\Events;

use GuWID\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use DB;
use Auth;

class SomeEvent extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    public function handle($action) {
        $user = Auth::user()->id;
        DB::table('audit')->insert([
            'user_id' => $user,
            'created_at' => date('Y-m-d H:i:s'),
            'action' => 'Logged in'
        ]);
    }
}
