<?php

namespace GuWID\Http\Controllers;

use GuWID\Http\Controllers\Controller;
use GuWID\Http\Requests\UploadRequest;
use Illuminate\Http\Request;
use Library\csv\CSVParser;
use Library\pts\Pts;
use GuWID\Audit;
use Storage;

class HomeController extends Controller
{

    private $pts;
    private $csv;

    public function __construct(Pts $pts, CSVParser $csv) {
        $this->pts = $pts;
        $this->csv = $csv;
    }

    public function index() {
        return view('home');
    }

    public function upload(UploadRequest $request) 
    {
        $audit = new Audit;
        $audit->user_id = $request->user()->id;
        $audit->created_at = date('Y-m-d H:i:s');
        $audit->action = 'Uploaded CSV';
        $audit->save();

        $file = $request->file('file')->getClientOriginalName();
        
        if (strpos(substr($file, -4), '.csv') === FALSE) {
            return redirect('dashboard')
                        ->withErrors('Not a valid csv')
                        ->withInput();
        } else {
            $request->file('file')->move('/var/www/html/guwid/storage/app/', $file);

            if ($request->radioPaymentType === 'WireCard') {
                $requiredHeaders = [
                    'FNC_CC_OCT',
                    'FunctionID',
                    'CC_TRANSACTION',
                    'TransactionID',
                    'GuWID',
                    'Amount',
                    'Currency',
                ];
            } else {
                $requiredHeaders = [
                    'gatewayRef',
                    'Amount',
                    'Merchant ID',
                    'Currency',
                    'MerchantExternalRef',
                    'Function ID',
                ];
            }
            
            $validHeaders = $this->isWireCard($request->radioPaymentType) ? $this->csv->parseWireCard($file, FALSE, TRUE) : $this->csv->parseIlixium($file, FALSE, TRUE);

            

            if ($validHeaders !== $requiredHeaders) {
                return redirect('home')
                        ->withErrors('CSV contains invalid headers')
                        ->withInput();
            }

            $accountNos = $this->isWireCard($request->radioPaymentType) ? $this->csv->parseWireCard($file) : $this->csv->parseIlixium($file);

            
            $list = implode(',', array_column($accountNos, 'accountno'));
            
            $this->pts->connect();
            // this is where we insert the accountno
            $spResults = $this->isWireCard($request->radioPaymentType) ? $this->pts->querySP($list, count($accountNos)) : $this->pts->querySP($list, count($accountNos), 'sp_Lucky247_ilixiumLookup');

            $allCSVResults = $this->isWireCard($request->radioPaymentType) ?  $this->csv->parseWireCard($file, TRUE) : $this->csv->parseIlixium($file, TRUE);

            $merged = [];
            if ($this->isWireCard($request->radioPaymentType)) {
                foreach ($spResults as $result) {
                    $resultXML = simplexml_load_string(substr($result['ResponseString'], 6));
                    
                    $usAccountNo = trim($result['USACCOUNTNO']);
                    $guWID = $resultXML->W_RESPONSE->W_JOB->FNC_CC_PURCHASE->CC_TRANSACTION->PROCESSING_STATUS->GuWID;
    				$processor = trim($result['Processor']);
    				$flag = trim($result['Flag']);

                        foreach ($allCSVResults as $row) {
                            if ($row['USACCOUNTNO'] == $usAccountNo) {
                                array_push($merged, [$row['FNC_CC_OCT'], $usAccountNo, $row['CC_TRANSACTION'], $row['TransactionID'], (string) $guWID, $row['Amount'], $row['Currency'], $processor, $flag]);
                            }
                        }
                    }

                    $requiredHeaders = ['FNC_CC_OCT' , 'FunctionID', 'CC_TRANSACTION', 'TransactionID', 'GuWID', 'Amount', 'Currency', 'Processor', 'Flag'];
                } else {
                    foreach ($spResults as $result) {
                        $resultXML = simplexml_load_string(substr($result['ResponseString'], 6));
                        $usAccountNo = trim($result['USACCOUNTNO']);
                        $gatewayRef = $resultXML->transaction->gatewayRef;
                        $merchantId = $resultXML->merchant->merchantId;
                        $currency = $resultXML->transaction->currency;
                        $amount = $resultXML->transaction->amount;
                    

                    foreach ($allCSVResults as $row) {
                        if ($row['Function ID'] == $usAccountNo) {
                            array_push($merged, [$gatewayRef, $row['Amount'], $merchantId, $currency, $row['MerchantExternalRef']]);
                        }
                    }
                }

                    $requiredHeaders = ['gatewayRef' , 'Amount', 'Merchant ID', 'Currency', 'MerchantExternalRef'];
                }
            }
			
            $this->csv->write($merged, $requiredHeaders, $file);

        }

        private function isWireCard($type)
        {
            return $type === 'WireCard';
        }


}
