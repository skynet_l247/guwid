<?php

namespace GuWID\Http\Requests;

use GuWID\Http\Requests\Request;
use Auth;

class UploadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
            return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required',
            'radioPaymentType' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'file.required' => 'Please upload a valid CSV',
            'radioPaymentType.required' => 'Please choose a payment type',
        ];
    }
}
