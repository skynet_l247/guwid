<?php

namespace Library\csv;

use League\Csv\Reader;
use League\Csv\Writer;

class CSVParser {

    private $inputCsv;
    private $accountnos = [];

    public function parseWireCard($file, $all = FALSE, $headersOnly = FALSE) {
        $this->inputCsv = Reader::createFromPath('/var/www/html/guwid/storage/app/' . $file);
        $this->inputCsv->setDelimiter(',');
        $headers = $this->inputCsv->fetchOne(0);

         if ($headersOnly) {
            return $headers;
        }

        $res = $this->inputCsv->setOffset(1)->fetch();

        if ($all) {
            $result = [];
            foreach ($res as $row) {
                array_push($result, ['FNC_CC_OCT' => trim($row[0]), 'USACCOUNTNO' => trim($row[1]), 'CC_TRANSACTION' => trim($row[2]), 'TransactionID' => trim($row[3]), 'Amount' => trim($row[5]), 'Currency' => trim($row[6])]);
            }

            return $result;
        }

        foreach ($res as $row) {
            // now we need to retrieve the accountno and query the db
            array_push($this->accountnos, ['accountno' => $row[1]]);
        }

        return count($this->accountnos) > 0 ? $this->accountnos : FALSE;
    }

    public function parseIlixium($file, $all = FALSE, $headersOnly = FALSE) {
        $this->inputCsv = Reader::createFromPath('/var/www/html/guwid/storage/app/' . $file);
        $this->inputCsv->setDelimiter(',');
        $headers = $this->inputCsv->fetchOne(0);

         if ($headersOnly) {
            return $headers;
        }

        $res = $this->inputCsv->setOffset(1)->fetch();

        if ($all) {
            $result = [];
            foreach ($res as $row) {
                array_push($result, 
                    [ 
                        'Amount' => trim($row[1]), 
                        'Currency' => trim($row[3]), 
                        'MerchantExternalRef' => trim($row[4]), 
                        'Function ID' => trim($row[5]),
                    ]
                );
            }

            return $result;
        }

        foreach ($res as $row) {
            // now we need to retrieve the accountno and query the db
            array_push($this->accountnos, ['accountno' => $row[5]]);
        }

        return count($this->accountnos) > 0 ? $this->accountnos : FALSE;
    }

    public function write(array $rows, $header, $file) {
        $writer = Writer::createFromFileObject(new \SplTempFileObject());
        $writer->setDelimiter(",");
        $writer->setNewline("\r\n");
        $writer->setOutputBOM(Writer::BOM_UTF8);
        $writer->insertOne($header);
        $writer->insertAll($rows);
        $writer->setEncodingFrom('ISO-8859-15');
        $writer->output($file);
    }

}