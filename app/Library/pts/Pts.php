<?php

namespace Library\pts;

use PDO;

class Pts {

    private $conn;

    public function connect() {
        try {
            $this->conn = new PDO("dblib:host=10.120.44.11;dbname=PTS2_PlayCentral","sa", "#+stas3awa@8DusP");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function selectAll() {
        $sql = "SELECT * FROM tb_guwid_temp_accounts";

        $stmt = $this->conn->prepare($sql);

        if ($stmt->execute()) {
            do {
                  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                } while ($stmt->nextRowset());
        }

        print_r($result);
    }

    public function insertAccountNo($accountNos) {
        $sql = 'INSERT INTO tb_guwid_temp_accounts (trans) VALUES ';
        $sqlPart = array_fill(0, count($accountNos), '(?)');
        $sql .= implode(', ', $sqlPart);

        $stmt = $this->conn->prepare($sql);

        $i = 0;
        foreach ($accountNos as $account) {
            $stmt->bindParam(++$i, $account['account'], PDO::PARAM_STR);
        }
        print_r($stmt);
        //if(!$stmt->execute()){echo "\nPDO::errorInfo():\n";}
    }

    public function querySP($list, $count, $sp = 'sp_Lucky247_guwidLookup') {
        $stmt = $this->conn->prepare("exec $sp @list=:list, @count=:count");
        $this->conn->query('SET ANSI_NULLS ON');
        $this->conn->query('SET ANSI_WARNINGS ON');
        $stmt->bindParam(':list', $list);
        $stmt->bindParam(':count', $count);
        if ($stmt->execute()) {
            do {
                  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                } while ($stmt->nextRowset());
        }

        return $result;
    }

    public function disconnect() {
        $this->conn = null;
    }

}
