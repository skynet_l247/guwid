<?php

namespace GuWID\Providers;

use Illuminate\Support\ServiceProvider;

use Library\pts\Pts;

class PTSProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Library\pts\Pts', function ($app) {
            return new Pts();
        });
    }
}
