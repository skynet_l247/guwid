<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            DB::table('users')->insert(
                [
                    'name' => 'Shaun Sparg',
                    'email' => 'shaun@regisms.com',
                    'password' => bcrypt('ss198509')
                ],
                [
                    'name' => 'Sean Auret',
                    'email' => 'sean@regisms.com',
                    'password' => bcrypt('12test')
                ],
                [
                    'name' => 'Shaheema Smith',
                    'email' => 'shaheema@regisms.com',
                    'password' => bcrypt('Sh@h33m@')
                ],
                [
                    'name' => 'David van der Merwe',
                    'email' => 'david@regisms.com',
                    'password' => bcrypt('Blink182!')
                ],
                [
                    'name' => 'Moenaba Khan',
                    'email' => 'moeneba@regisms.com',
                    'password' => bcrypt('welcome1')
                ],
                [
                    'name' => 'Aran Badassy',
                    'email' => 'aran@regisms.com',
                    'password' => bcrypt('#Aran001')
                ]				
            );
    }
}
