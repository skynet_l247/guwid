@extends('master')
@section('title', 'login')
@section('content')
    <div class="login-wrap">
        <form class="form-horizontal" method="POST" action="/auth/login">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="login_email" class="col-lg-2">Email address</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="email" id="login_email">
                </div>
            </div>

            <div class="form-group">
                <label for="login_password" class="col-lg-2">Password</label>
                <div class="col-lg-10">
                    <input type="password" class="form-control" name="password" id="login_password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary pull-right">Sign in</button>
                </div>
            </div>

        </form>
    </div>
    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
@endsection