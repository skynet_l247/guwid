<footer class="footer">
      <div class="container">
        <p class="text-muted">&copy {{ date('Y') }} Regis Management</p>
      </div>
    </footer>