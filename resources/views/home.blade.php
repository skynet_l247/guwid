@extends('master')
@section('title', 'home')
@section('content')
<div class="home-wrap">
    <div class="row" style="margin-left: 50px;">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <form class="form-horizontal" method="POST" action="home/upload" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <label for="csv-input-file">File input</label>
                    <input type="file" id="csv-input-file" name="file">
                    <p class="help-block">Upload a valid csv file.</p>
                    <label class="radio-inline">
                      <input type="radio" name="radioPaymentType" id="radioPaymentType1" value="WireCard"> WireCard
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="radioPaymentType" id="radioPaymentType" value="Ilixium"> Ilixium
                    </label>
              <div class="form-group">
                <button type="submit" class="btn btn-primary" id="btn-upload" style="display: inline-block; margin-left: 10px; margin-top: 10px;">Upload</button>
              </div>
            </form>  
        </div> 
    </div>
    <div class="alert alert-success hide" role="alert" id="upload-alert">Please wait! Processing request!</div>
</div>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul style="list-style: none;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
@endsection